/*
 * Copyright 2017-2018 E257.FI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import better.files._
import org.scalatest.FlatSpec
import resource._

class GitDBTest extends FlatSpec {

  behavior of "gitdb"
  it should "handle sequential commits" in {
    val repopath = "tests/db-test1.git"
    val rp = File(repopath)
    rp.delete(true)

    val repository = GitDBTester.getRepository(repopath)
    try {
      repository.create(true)
      val u = new GitDB(repository)
      try {
        GitDBTester.addTiming(u, 100)
        GitDBTester.racing(u)
      } catch {
        case e: Exception =>
          e.printStackTrace()
          assert(false)
      } finally if (u != null) u.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
        assert(false)
    } finally if (repository != null) repository.close()
  }

  it should "handle racing" in {
    val repopath = "tests/db-test2.git"
    val rp = File(repopath)
    rp.delete(true)

    val repository = GitDBTester.getRepository(repopath)
    try {
      repository.create(true)
      val u = new GitDB(repository)
      try {
        GitDBTester.racing(u)
      } catch {
        case e: Exception =>
          e.printStackTrace()
          assert(false)
      } finally if (u != null) u.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
        assert(false)
    } finally if (repository != null) repository.close()
  }

  it should "error with multiple repo instances" in {
    val repopath = "tests/db-test3.git"
    File(repopath).delete(true)

    val result = managed(GitDBTester.getRepository(repopath)).map(repository => {
      repository.create(true)

      val mr1 = managed(new GitDB(repository)).map(db1 => {
        val mr2 = managed(new GitDB(repository)).map(db2 => db2)
        // following line is no-op, because above call should throw an exception
        mr2.either.isRight
      })
      assert(mr1.either.isLeft)
      val ex = mr1.either.left.get.head
      println(ex.getMessage)
      assert(ex.getMessage.startsWith("Can not get lock:"))
    })
    // this is checking inner assert condition -> right
    assert(result.either.isRight)
  }

  it should "work as managed resource" in {
    val repopath = "tests/db-test-managed.git"
    File(repopath).delete(true)

    val result = managed(GitDBTester.getRepository(repopath)).map(repository => {
      repository.create(true)

      val r1 = managed(new GitDB(repository)).map(db => {
        db.commit(new DataItem("foo", "bar"))
        println("commit-1-1")
      })
      assert(r1.either.isRight)

      val r2 = managed(new GitDB(repository)).map(u => {
        u.commit(new DataItem("foo2-1", "bar2-1"))
        println("commit-2-1")
        u.commit(new DataItem("foo2-2", "bar2-2"))
        println("commit-2-2")
      })
      assert(r2.either.isRight)
    })
    assert(result.either.isRight)
  }
}
