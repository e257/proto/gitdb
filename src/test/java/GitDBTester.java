/*
 * Copyright 2017-2018 E257.FI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import org.eclipse.jgit.api.errors.ConcurrentRefUpdateException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.UnmergedPathsException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class GitDBTester {

    private static class Committer implements Runnable {
        private GitDB gitdb;
        private String name;
        private int sleep;
        private volatile Throwable ex;

        public Committer(GitDB gitdb, String name, int sleep) {
            this.gitdb = gitdb;
            this.name = name;
            this.sleep = sleep;
        }

        public void throwError() throws Throwable {
            if (ex != null)
                throw ex;
        }

        @Override
        public void run() {
            Random timer = new Random();
            for (int i = 0; i < 20; i++) {
                try {
                    ZonedDateTime ts = ZonedDateTime.now();
                    String tsStr = ts.format(DateTimeFormatter.ISO_INSTANT);

                    RevCommit commit = gitdb.commit(
                            new DataItem(name + "/foobar-" + tsStr, name + ": " + tsStr + "\n") {
                                @Override
                                public String getPath() {
                                    try {
                                        System.out.print(name.toUpperCase() + " Sleeping ...");
                                        Thread.sleep(sleep);
                                        System.out.println("done");
                                    } catch (InterruptedException e) {
                                    }
                                    return super.getPath();
                                }
                            });
                    System.out.println(name.toUpperCase() + ": commit: " + commit.getId().getName());
                    Thread.sleep(timer.nextInt(7));

                } catch (IOException e1) {
                    ex = e1;
                } catch (WrongRepositoryStateException e1) {
                    ex = e1;
                } catch (NoHeadException e1) {
                    ex = e1;
                } catch (ConcurrentRefUpdateException e1) {
                    ex = e1;
                } catch (UnmergedPathsException e1) {
                    ex = e1;
                } catch (InterruptedException e1) {
                    ex = e1;
                }
            }
        }
    }

    public static void main(String[] args) throws Throwable {
        String repopath = "../../tmp/db-test.git";
        try (Repository repository = getRepository(repopath)) {
            repository.create(true);

            try (GitDB u = new GitDB(repository)) {
                try {
                    addTiming(u, 100);
                    racing(u);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Repository getRepository(String repopath) throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        return builder
                .setMustExist(false)
                .setBare()
                .setGitDir(new File(repopath))
                .build();
    }

    public static void addTiming(GitDB u, int rounds) throws IOException, InterruptedException, WrongRepositoryStateException, NoHeadException, ConcurrentRefUpdateException, UnmergedPathsException {

        ZonedDateTime ts = ZonedDateTime.now();
        String startTsStr = ts.format(DateTimeFormatter.ISO_INSTANT);
        System.out.println("Start: " + startTsStr);

        int i = 1;
        DataItem data = new DataItem(String.format("100-beers/%s/start-%05d", startTsStr, i), "start: " + startTsStr + "\n");
        u.commit(data);

        for (; i < rounds; i++) {
            data = new DataItem(String.format("100-beers/%s/beer-%06d", startTsStr, i), String.format("beer: %d\nrun: %s\n", i, startTsStr));
            u.commit(data);
        }

        ts = ZonedDateTime.now();
        String endTsStr = ts.format(DateTimeFormatter.ISO_INSTANT);
        data = new DataItem(String.format("100-beers/%s/end-%05d", startTsStr, i), "" +
                "start: " + startTsStr + "\n" +
                "end:   " + endTsStr + "\n" +
                "beers: " + String.format("%d", i));
        u.commit(data);
        System.out.println("End:   " + endTsStr);
    }

    public static void racing(GitDB u) throws Throwable {

        Committer fastRunner = new Committer(u, "fast", 1);
        Committer slowRunner = new Committer(u, "slow", 12);
        Thread fast = new Thread(fastRunner);
        Thread slow = new Thread(slowRunner);

        slow.start();
        fast.start();

        slow.join();
        fast.join();

        fastRunner.throwError();
        slowRunner.throwError();
    }
}
