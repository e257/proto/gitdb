
name := "gitdb"
organization := "fi.e257"
version := "1.0"
scalaVersion := "2.12.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val betterFilesVersion = "3.7.0"
  val jgitVersion = "5.2.0.201812061821-r"
  val scalaArmVersion = "2.0"
  val scalaTestV  = "3.0.5"


  Seq(
    "org.eclipse.jgit" % "org.eclipse.jgit" % jgitVersion,
    "com.github.pathikrit" %% "better-files" % betterFilesVersion,

    // TEST scope
    "com.jsuereth" %% "scala-arm" % scalaArmVersion % "test", 
    "org.scalatest" %% "scalatest" % scalaTestV % "test"
  )
}

