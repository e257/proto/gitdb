#!/bin/bash
# vim: tabstop=4 shiftwidth=4 softtabstop=4 smarttab expandtab autoindent

#set -x

if [ $# != 1 ]; then
	echo "Usage: $0 <1E3 | 1E4 | 1E5 | 1E6>"
	exit 1
fi

name=$1


#git init --bare "$name.git"
#git clone "$name.git"
#mkdir -p "$name"

cd "t1"

touch add-$name.txt
git add add-$name.txt
git commit -m "initial $name"
 
#mkdir -p $name

for i in 0 1 2 3 4 5 6 7 8 9; do
for j in 0 1 2 3 4 5 6 7 8 9; do
for k in 0 1 2 3 4 5 6 7 8 9; do
for l in 0 1 2 3 4 5 6 7 8 9; do

    round="$i$j$k$l"
    echo "Perf: doing $name, round: $round"

    echo "data: $round" > "data/$name-$round.txt"
    tsStart=$(date +%s.%N)
    git add "data/$name-$round.txt"
    git commit -m "txns: $name: $round"
    tsEnd=$(date +%s.%N)

    echo $tsStart
    echo $tsEnd
    echo
    # make sure that git time stamps are distinct
done
done
done
done

#git push --set-upstream origin $name
git push

